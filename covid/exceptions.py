# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""


class CovidMatcherError(Exception):
    pass


class DatetimeError(Exception):
    pass


class OffsetError(Exception):
    pass
