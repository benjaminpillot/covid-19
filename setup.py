# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""

from setuptools import setup, find_packages

import covid


with open("README.md", 'r') as fh:
    long_description = fh.read()

with open("requirements.txt") as req:
    install_req = req.read().splitlines()

setup(name='pycovid-19',
      version=covid.__version__,
      description='Covid Python plots and maps',
      long_description=long_description,
      long_description_content_type="text/markdown",
      url='http://framagit.org/benjaminpillot/covid-19',
      author='Benjamin Pillot',
      author_email='benjaminpillot@riseup.net',
      install_requires=install_req,
      python_requires='>=3.9',
      license='GNU GPL v3.0',
      packages=find_packages(),
      zip_safe=False)
